// Init kafka client
const {Kafka} = require('kafkajs');
const kafka = new Kafka({
    clientId: 'consumer',
    brokers: [`${process.env.KAFKA_HOST || "kafka"}:${process.env.KAFKA_PORT || 9092}`]
});
const consumer = kafka.consumer({groupId: process.env.GROUP_ID || "consumer-group"});
const admin = kafka.admin();

// Init express with socket.io
const express = require('express');
const app = express();
const http = require('http');
const server = http.createServer(app);

const {Server} = require("socket.io");
const io = new Server(server);

app.use(express.static('dist'));
server.listen(8080, () => {
    console.log('listening on *:8080');
});

// App state management
/**
 * Represents a class, with its id, maxGauge, name and history
 */
class Class {
    constructor(id, maxOccupancy, name) {
        this.id = id;
        this.maxOccupancy = maxOccupancy;
        this.name = name;
        this.history = [];
        this.strikes = 0;
    }
}

/**
 * Represents a day in the history of the government
 */
class GovDay {
    constructor(date, maxGauge) {
        this.date = date;
        this.maxGauge = maxGauge;
    }
}

/**
 * Represents a day in the history of a class
 */
class ClassDay {
    constructor(date, occupancy) {
        this.date = date;
        this.occupancy = occupancy;
    }
}

/**
 * Represents every classes in the region
 * @type {Array <Class>}
 */
let classes;
/**
 * Represents the government history
 * @type {Array <GovDay>}
 */
let history;

/**
 * Represents the number of days allowed to apply the new gauge
 */
let applicationTime;

/**
 * Represents the message from which we should start the simulation
 */
let firstOffset;

/**
 * Generates a random number between 0 and max
 * @param max
 * @returns {number}
 */
const generateRandomNumber = (max) => {
    return Math.floor(Math.random() * Math.floor(max));
}

/**
 * This function initializes app data and connects to kafka topics
 *
 * @returns {Promise<void>}
 */
const init = async () => {
    // Init app data
    applicationTime = process.env.DEFAULT_APPLICATION_TIME || 7;
    history = [];
    classes = [];
    const nbClasses = process.env.NB_CLASSES || 5;
    for (let i = 0; i < nbClasses; i++) {
        classes.push(new Class(i, generateRandomNumber(45), `Class ${i}`));
    }

    // Init kafka connexion
    await consumer.connect();
    await consumer.subscribe({topic: process.env.TOPIC_MAX_GAUGE || 'maxgauge'});
    await consumer.subscribe({topic: process.env.TOPIC_APPLICATION_TIME || 'applicationtime'});
    firstOffset = (await admin.fetchTopicOffsets(process.env.TOPIC_MAX_GAUGE || 'maxgauge'))[0].offset;
}

const run = async () => {
    consumer.run({
        eachMessage: async ({topic, partition, message}) => {
            switch (topic) {
                case process.env.TOPIC_MAX_GAUGE:
                    if (message.offset > firstOffset) {
                        history.push(new GovDay(parseInt(message.key.toString()), parseInt(message.value.toString())));
                        newCycle();
                    }
                    break;
                case process.env.TOPIC_APPLICATION_TIME:
                    applicationTime = message.value;
                    break;
            }
        }
    })
}

const newCycle = () => {
    // Retrieve the last day's data to use it more easily
    const date = history[history.length - 1].date;
    const maxGauge = history[history.length - 1].maxGauge;

    let graph1 = {
        maxGauge,
        date: `${date} ${(date % process.env.WEEK_DURATION) === 0 ? "(début de la semaine)" : ""}`,
        classes: []
    };
    let graph2 = {
        maxStrikes: process.env.MAX_STRIKES || 3,
        date,
        classes: []
    };

    classes.forEach(classItem => {
        const newGauge = generateRandomNumber(classItem.maxOccupancy); // Generate a new gauge
        classItem.history.push(new ClassDay(date, newGauge)); // Add the new gauge to the class history

        // Check if the class gets a strike
        if (newGauge > maxGauge && // If the class occupancy is greater than the government's max gauge
            date % process.env.WEEK_DURATION >= applicationTime) { // And if we're past the allowed application time
            classItem.strikes++;
        }

        graph1.classes.push({name: classItem.name, occupancy: newGauge, maxOccupancy: classItem.maxOccupancy});
        graph2.classes.push({name: classItem.name, strikes: classItem.strikes});
    });

    let graph3 = { gov: [], classes: []};
    for (let i=0; i<(process.env.HISTORY_DURATION || 10); i++) {
        graph3.gov.push({
            date: history[history.length - i - 1].date,
            maxGauge: history[history.length - i - 1].maxGauge
        });
        graph3.classes.push({
            date: history[history.length - i - 1].date,
            classes: classes.map(
                classItem => classItem.history[classItem.history.length - i - 1].occupancy
            ).reduce((acc, curr) => acc + curr, 0) / classes.length
        });
    }
    graph3.gov.reverse();
    graph3.classes.reverse();

    io.emit('graph1', graph1);
    io.emit('graph2', graph2);
    io.emit('graph3', graph3);
}

init().then(run).catch(console.error);
