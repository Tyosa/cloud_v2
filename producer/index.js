const {Kafka} = require('kafkajs');

let gauge = [];

const kafka = new Kafka({
    clientId: 'producer',
    brokers: [`${process.env.KAFKA_HOST || "kafka"}:${process.env.KAFKA_PORT || 9092}`]
});
const producer = kafka.producer();
const admin = kafka.admin();

/**
 * This function generates a new gauge between 1 and 30
 *
 * @returns {number}
 */
const generateGauge = () => {
    return Math.floor(Math.random() * 30) + 1;
}

/**
 *
 * This function generates a new day in the simulation
 * If the new day is the beginning of a new week, it generates a new gauge
 *
 * @returns {{key: string, value: string}}
 */
const generateNewDay = () => {
    let day = {key: gauge.length.toString(), value: ''};
    if (gauge.length % (process.env.WEEK_DURATION || 5) === 0) { // If we have a new week
        day.value = generateGauge().toString();
    } else {
        day.value = gauge[gauge.length - 1].value
    }
    gauge.push(day);
    return day;
}

/**
 *
 * This function is used to generate and send a message to the Kafka topic
 *
 * @returns {Promise<>}
 */
const sendGaugeMessage = () => {
    const message = generateNewDay();
    return producer.send({
        topic: process.env.TOPIC_MAX_GAUGE || 'jaugemax',
        messages: [
            message
        ]
    });
}

const sendApplicationTimeMessage = () => {
    const message = {key: '0', value: process.env.DEFAULT_APPLICATION_TIME || 2};
    return producer.send({
        topic: process.env.TOPIC_APPLICATION_TIME || 'applicationtime',
        messages: [
            message
        ]
    });
}
/**
 *
 * This function is called when the application starts.
 * It is responsible for initializing the simulation.
 *
 * @returns {Promise}
 */
const run = async () => {
    await producer.connect();
    setInterval(sendGaugeMessage, process.env.DAY_DURATION * 1000 || 1000); // Send the gauge every day
    setInterval(sendApplicationTimeMessage, (process.env.DAY_DURATION * 1000 || 1000) * (process.env.WEEK_DURATION || 5)); // Send the application time every week
}

const init = async () => {
    await admin.deleteTopics({
        topics: [process.env.TOPIC_MAX_GAUGE || 'jaugemax', process.env.TOPIC_APPLICATION_TIME || 'applicationtime']
    });
    await admin.createTopics({
        topics: [{
            topic: process.env.TOPIC_MAX_GAUGE || 'jaugemax',
            numPartitions: 1,
            replicationFactor: 1
        }, {
            topic: process.env.TOPIC_APPLICATION_TIME || 'applicationtime',
            numPartitions: 1,
            replicationFactor: 1
        }]
    });
}

init().catch(console.error).then(run).catch(console.error);