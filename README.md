# Cloud Project

## How to run
```sh
docker-compose up -d
```

Each region is then available on `http://localhost/<region>`, with `<region>` being either region1, region2 or region3.

## Project overview

This project aims to create a simple distributed app. It applies the following structure : 

- producer : Generates a new gauge each day and sends it
- consumer : Teceives the new gauge and stores it, then generate its own occupancy and display it 
- kafka : The consumer and producer are connected through kafka. Data is sent to the topics `$TOPIC_MAX_GAUGE` and `$TOPIC_APPLICATION_TIME`, both stored in environment variables. The first one receives the new day's gauge, and the second one stores the application time if it is changed.
- nginx : Used as a reverse proxy, so we can use only one port and still access each deployed consumer.

Both producer and consumer are Node.js programs. The consumer exposes an Express server on port 8080 to display the data on a web interface.

